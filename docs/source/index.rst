


International Course in Microbial Ecology ICME9
===============================================

Welcome to EMBO Practical Course **"Microbial ecology: Hands-on training in prokaryotic and eukaryotic metagenomics (ICME-9)"**, Bruxelles 2-7 September 2018.

This EMBO Practical Course is aimed at PhD students, post-docs, and researchers with some experience in the field of metagenomics and who are seeking to improve their skills. Beginners are also welcome and will be introduced to Linux command Shell and main command line. For further information about the corse, visit http://meetings.embo.org/event/18-metagenomics

Part of the practical is based of the software repository https://gitlab.com/andrea.franzetti/icme9, which contains all the third-part software and dependencies to run a complete bioinformatic analysis of a metagenomics dataset. These elaborations are supposed to be ran on  GALILEO cluster of CINECA (http://www.hpc.cineca.it/hardware/galileo)

* Quality Control: FastQC (https://www.bioinformatics.babraham.ac.uk/projects/fastqc/)
* Quality Filtering: Sickle (https://github.com/najoshi/sickle)
* Assembly: IDBA-UD (https://github.com/loneknightpy/idba)
* Gene prediction: Prodigal (https://github.com/hyattpd/Prodigal)
* Annotation: DIAMOND (https://github.com/bbuchfink/diamond)
* Alignment: Bowtie2 (http://bowtie-bio.sourceforge.net/bowtie2/index.shtml)
* Alignment file manipulation: Samtools, Bedtools (http://samtools.sourceforge.net/; http://bedtools.readthedocs.io/en/latest/)
* Taxonomic assignments and parsing: MEGAN, Krona, in-house scritps (http://ab.inf.uni-tuebingen.de/software/megan6/; https://github.com/marbl/Krona/wiki)
* Graphical output: Krona


The present documentation helps the students to run the whole pipiline on the computer cluster.

Contents:

.. toctree::
   :maxdepth: 2
   
   cineca
   quality   	
   trimming
   assembly
   binning
   prodigal
   annotation
   mapping 
   parsing
   mining	
