Connecting to Cineca
====================

To open a ssh connection to the computer cluster GALILEO on CINECA. If you have a Mac or a PC running Linux, start the terminal (black screen icon) and type::

	ssh username@login.galileo.cineca.it


and give your password when prompted. Now you get a welcoming message from CINECA to inform you that you have successfully logged in.

Now follow the tutor instructions and have a look to the GALILEO file system


**Clone software repository**

Clone the scripts on your personal SCRATCH AREA::
	
	cd $CINECA_SCRATCH
	git clone https://gitlab.com/andrea.franzetti/icme9.git

**Install software and dependencies**::

	cd icme9
	./built_deps.sh

**Install MEGAN tools**::

	module load jre
	chmod a+x ./deps/megan/MEGAN_Community_unix_6_12_3.sh
	./deps/megan/MEGAN_Community_unix_6_12_3.sh

and follow teacher instructions.

**Start an interactive session on a node of your own**

Now you need to start an interactive session and reserve 12 cores in a single node on the GALILEO server to run your analyses. One GALILEO node contains 36 cores and 120 Gb of RAM.

To start an interactive session type::

	srun -p gll_usr_prod --account train_embo2018 --reservation=s_tra_embo2018 --time 08:00:00 --mem=28000 -N 1 -n 12 --pty /bin/bash
	cd $CINECA_SCRATCH/icme9


**Setting variables**

In the file "conf_cluster.conf" you can set environment variables and bioinformatic parameters for the analysis. 

Open the file with an editor::

	nano conf_cluster.conf 

and modify it following the instructions of the instructor.

To make the changes active source the file::

	source conf_cluster.conf 
