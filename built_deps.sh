#! /bin/bash

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"

echo -e "\n This script will install the dependencies necessary for the
metagenomic pipeline.\n" 

echo -e "PLEASE NOTE THAT THIS SCRIPT HAS BEEN WRITTEN IN APRIL 2017\n"
echo -e "To install more recent version, pleas edit the script\n"

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"

echo -e "#================== SETTIN ENV  ==================="
echo -e "\n Creating a deps folder in the working directory ($(pwd))"

DEPS=$(pwd)/deps
mkdir -p $DEPS 

cd $DEPS

echo -e "\n Creating a folder that will contain sl to binaries \n"

SLBIN=$DEPS/SLBIN
echo $SLBIN
mkdir -p $SLBIN

echo -e "\n Creating a folder that will contain the tar arichives \n"

TAR=$DEPS/archives
echo $TAR
mkdir -p $TAR

echo -e "================== SICKLE ===================\n"
echo -e "I'm going to install the following software: -sickle version 1.33"

wget -O sickle_1.3.3.tar.gz https://github.com/najoshi/sickle/archive/v1.33.tar.gz
tar -zxvf sickle_1.3.3.tar.gz
cd sickle-1.33
make

ln -s $(pwd)/sickle $SLBIN/sickle

cd ..

mv $DEPS/sickle_1.3.3.tar.gz $TAR

echo -e "================== IDBA UD ===================\n"
echo -e "I'm going to install the following software: Idba-ud version 1.1.3"

wget -O idba_1.1.3.tar.gz  https://github.com/loneknightpy/idba/releases/download/1.1.3/idba-1.1.3.tar.gz
tar -zxvf idba_1.1.3.tar.gz

cd idba-1.1.3
./configure
make

ln -s $(pwd)/bin/ $SLBIN/idba_bin

cd ..

mv $DEPS/idba_1.1.3.tar.gz $TAR

echo -e "================== PRODIGAL 2.6.3 ===================\n"
echo -e "I'm going to install the following software: Prodigal2.6.3"

wget -O Prodigal_2.6.3.tar.gz https://github.com/hyattpd/Prodigal/archive/v2.6.3.tar.gz
tar -zxvf Prodigal_2.6.3.tar.gz

cd Prodigal-2.6.3

make

ln -s $(pwd)/prodigal $SLBIN/prodigal

cd ..

mv $DEPS/Prodigal_2.6.3.tar.gz $TAR

echo -e "================== FragGeneScan 1.30 ===================\n"
echo -e "I'm going to install the following software: Prodigal 1.3.0"

wget -O FragGeneScan.tar.gz --no-check-certificate https://sourceforge.net/projects/fraggenescan/files/FragGeneScan1.30.tar.gz/download
tar -zxvf FragGeneScan.tar.gz

cd FragGeneScan1.30
make
make clean
make fgs

ln -s $(pwd) $SLBIN/FragGeneScan

cd ..

mv $DEPS/FragGeneScan.tar.gz $TAR

echo -e "================== DIAMOND 0.8.36 ===================\n"
echo -e "I'm going to install the following software: Diamond version 0.8.36"

wget -O diamond_0.8.36.tar.gz  https://github.com/bbuchfink/diamond/releases/download/v0.8.36/diamond-linux64.tar.gz
mkdir -p $DEPS/diamond
mv diamond_0.8.36.tar.gz $DEPS/diamond

cd diamond
tar -zxvf diamond_0.8.36.tar.gz

ln -s $(pwd)/diamond $SLBIN/diamond

cd ..

echo -e "================== KAIJU 0.8.36 ===================\n"
echo -e "I'm going to install the following software: Kaiju version 1.5.0"

wget -O kaiju_1.5.0.tar.gz https://github.com/bioinformatics-centre/kaiju/releases/download/v1.5.0/kaiju-1.5.0-linux-x86_64.tar.gz
tar -zxvf kaiju_1.5.0.tar.gz
ln -s $DEPS/kaiju-v1.5.0-linux-x86_64-static/bin/kaiju $SLBIN/kaiju

mv $DEPS/kaiju_1.5.0.tar.gz $TAR

echo -e "================== SAMTOOLS 1.4 ===================\n"
echo -e "I'm going to install the following software: Samtools version 1.4"

wget -O samtools_1.4.tar.bz2 samtools https://github.com/samtools/samtools/releases/download/1.4/samtools-1.4.tar.bz2
tar -xjvf samtools_1.4.tar.bz2

cd samtools-1.4

./configure --disable-lzma
make

ln -s $(pwd)/samtools $SLBIN/samtools

cd ..

mv $DEPS/samtools_1.4.tar.bz2 $TAR

echo -e "================== BEDTOOLS 2.26.0 ===================\n"
echo -e "I'm going to install the following software: Bedtools version 2.26.0"

echo -e "To be verified on MARCONI"
wget -O bedtools_2.26.0.tar.gz https://github.com/arq5x/bedtools2/releases/download/v2.26.0/bedtools-2.26.0.tar.gz
tar -zxvf bedtools_2.26.0.tar.gz

cd bedtools2

make

ln -s $(pwd)/bin/bedtools $SLBIN/bedtools

cd ..

mv $DEPS/bedtools_2.26.0.tar.gz $TAR

echo -e "================== BOWTIE 2 ===================\n"
echo -e "I'm going to install the following software: Bowtie version 2"

wget -O bwt.zip https://downloads.sourceforge.net/project/bowtie-bio/bowtie2/2.3.1/bowtie2-2.3.1-legacy-linux-x86_64.zip

unzip bwt.zip

ln -s $(pwd)/bowtie2-2.3.1-legacy/ $SLBIN/bowtie2_bin

mv $DEPS/bwt.zip $TAR

echo -e "================== Spades  ===================\n"
echo -e "I'm going to install the following software: Spades Version 3.11.0"

wget http://cab.spbu.ru/files/release3.11.0/SPAdes-3.11.0-Linux.tar.gz

tar -xzf SPAdes-3.11.0-Linux.tar.gz

ln -s $(pwd)/SPAdes-3.11.0-Linux/bin/spades.py $SLBIN/spades

mv $DEPS/SPAdes-3.11.0-Linux.tar.gz $TAR

#==== python scripts ====

echo -e "---Setting assemstats2.py---"

cd $DEPS

cd ../

module load autoload git
module load autoload python/3.6.4
echo $(pwd)

echo -e "Creating virtualenv 'venv' \n"

virtualenv -p python3 env
source env/bin/activate

echo -e "Installing pandas in the virtualenv"
pip install pandas

deactivate


echo -e "================== MEGAN TOOLS ===================\n"

cd $DEPS

if [ -d "$DEPS"/megan ]; then
    rm -rf "$DEPS"/megan/*
else
    mkdir -p megan
fi

cd megan

wget http://ab.inf.uni-tuebingen.de/data/software/megan6/download/MEGAN_Community_unix_6_12_3.sh

printf "o\n1\n$(pwd)\ny\n1,2,3\nn\n5\n${MEM}\nn" | bash MEGAN_Community_unix_6_12_3.sh

ln -s $(pwd) $SLBIN/megan

ln -s $(pwd)/tools/blast2rma $SLBIN/blast2rma
ln -s $(pwd)/tools/rma2info $SLBIN/rma2info

cd $DEPS

echo -e "================== KRONA 2-7 ===================\n"
echo -e "I'm going to install the following software: Krona"
wget -O KronaTools-2.7.tar https://github.com/marbl/Krona/releases/download/v2.7/KronaTools-2.7.tar

tar -xvf KronaTools-2.7.tar
mv *tar $TAR
cd KronaTools-2.7
mkdir bin
./install.pl --prefix ./
./updateTaxonomy.sh
#./updateAccessions.sh

cd $DEPS

ln -s $DEPS/KronaTools-2.7/bin $SLBIN/Krona

echo -e "================== Maxbin ===================\n"
echo -e "I'm going to install Maxbin and its dependecy hmmer "

cd $DEPS

wget -O MaxBin.tar.gz --no-check-certificate https://downloads.sourceforge.net/project/maxbin/MaxBin-2.2.3.tar.gz
wget -O hmmer.tar.gz http://eddylab.org/software/hmmer3/3.1b2/hmmer-3.1b2-linux-intel-x86_64.tar.gz

tar -zxvf MaxBin.tar.gz
tar -zxvf hmmer.tar.gz

echo -e "---Building hmmer---"
mv hmmer-3.1b2-linux-intel-x86_64 hmmer

cd hmmer
./configure
make
make check
cd $DEPS
ln -s $DEPS/hmmer/binaries $SLBIN/hmmer

echo -e "---Building MaxBin---"

mv MaxBin-2.2.3 MaxBin

cd MaxBin/src/

make

cd $DEPS

ln -s $DEPS/MaxBin $SLBIN/MaxBin

echo -e "================== FINISH ===================\n"

echo -e "Now you have al the dipendecies to run on GALILEO and MARCONI"
