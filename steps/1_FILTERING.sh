#!/bin/bash

cd $SLURM_SUBMIT_DIR 

source $CONF

echo $FILT_READS
mkdir -p $FILT_READS
#cd $FILR_READS

echo "Performing Filtering"
echo "`date`: filtering "

echo -e "\nCreating and exporting output folders: filtered\n"

echo "-------------------------------------------------------------------------"
echo -e "Quality filtering....\n"

echo $SAMPLE_LIST

for i in $SAMPLE_LIST; do
    #Number of reads
    echo "Sample $i"
    echo "Reads $i"
    grep "@" $INPUT_DIR/$i.R1.fastq | wc -l

    $LNBIN/sickle pe -q 30 \
    		  -l 80 \
    		  -f $INPUT_DIR/$i.R1.fastq \
    		  -r $INPUT_DIR/$i.R2.fastq \
    		  -t sanger \
    		  -o $FILT_READS/filtered.$i.R1.fastq \
    		  -p $FILT_READS/filtered.$i.R2.fastq \
    		  -s $FILT_READS/filtered.single.$i.fastq \
    		  -q $AV_READS_Q \
    		  -l $AV_READS_L

    echo "Sample $i"
    echo "Filtered reads $i"
    grep "@" $INPUT_DIR/$i.R1.fastq | wc -l
done
echo "-------------------------------------------------------------------------"
