#!/bin/bash
#importing java

module load autoload jre

cd $SLURM_SUBMIT_DIR

source $CONF

echo "Performing Gene Pred"

echo "Create output folder: Gene Prediction"
mkdir -p $wd/$PRJ_NAME/3_Gene_Prediction

echo "Gene prediction with Prodigal...."

echo -e "\n `date`: Running Prodigal..."


case $GENE_PREDICTOR in 
    1)

	echo -e "\n `date` Predicting genes with prodigal"
	$LNBIN/prodigal -i $ASSEMB_READS/d.$PRJ_NAME.*/contig.fa \
	     -d $GENE_PRED/prodigal.idba.$PRJ_NAME.fasta \
	     -a $GENE_PRED/prodigal.idba.$PRJ_NAME.faa \
	     -o $GENE_PRED/prodigal.idba.$PRJ_NAME.gbk \
	     -p meta

	echo "\n `date`: Running Annotation with Diamond..."
	$LNBIN/diamond blastp --query $GENE_PRED/prodigal.*.$PRJ_NAME.faa \
	     --db $DB/nr.dmnd \
	     -p 20 \
	     -f 6 \
	     --sensitive \
	     --out $GENE_PRED/diamond.prodigal.idba.$PRJ_NAME.m8
        ;;
	
    2)
	echo "\n `date`: Predicting genes with frag gene scan"
	$LNBIN/FragGeneScan1.30/run_FragGeneScan.pl -genome=$ASSEMB_READS/d.$PRJ_NAME.*/contig.fa \
	    -out=$GENE_PRED/FGS.idba.$PRJ_NAME \
	    -complete=0 \
	    -train=illumina_10
	
	echo "\n `date`: Running Annotation with Diamond..."
	$LNBIN/diamond blastp --query $GENE_PRED/FGS.idba.$PRJ_NAME.faa \
	    --db $DB/uniprot.dmnd \
	    -p 20 \
	    -f 6 \
	    --sensitive \
	    --out $GENE_PRED/diamond.FGS.idba.$PRJ_NAME.m8
	    
	;;
esac

echo -e "`date`: Diamond Ended \n"

echo -e "Parsing Diamond output: Meganize .m8 file"


#--- This is chuck of code substitue the call to blast2rma ---

#The jars called here are available in the MEGAN6 download page:
#http://ab.inf.uni-tuebingen.de/data/software/megan6/download/welcome.html

cd $LNBIN/megan

bin_dir=$LNBIN/megan/tools

jars_dir=$bin_dir/../jars #harcoded

vmOptions=`grep "^-" $bin_dir/../MEGAN.vmoptions`
classpath="$jars_dir/MEGAN.jar:$jars_dir/MALT.jar:$jars_dir/data.jar:"

#flags needed by java classes (?!)

java_flags="-server -Duser.language=en -Duser.region=US -Djava.awt.headless=true $vmOptions"

#arguments of java files
options="--format BlastTab -i $GENE_PRED/diamond.*.$PRJ_NAME.m8 -a2t $DB/prot_acc2tax*.abin -a2seed $DB/acc2seed-May2015XX.abin -o $GENE_PRED/temp_tax_seed.rma"

java $java_flags -cp "$classpath" megan.tools.BLAST2RMA6   $options

#--- End of blast2rma ---

echo -e "Extracting informations from meganized file: Taxonomy"

options="--in $GENE_PRED/*rma -u false -v -l -m -r2c Taxonomy -mro false -bo --out $GENE_PRED/id_gene2tax.$PRJ_NAME.tab" 
java $java_flags -cp "$classpath" megan.tools.RMA2Info   $options > $GENE_PRED/id_gene2tax.$PRJ_NAME.tab

echo -e "Extracting informations from meganized file: Seed names"

options="-i $GENE_PRED/temp_tax_seed.rma -u false -r2c SEED -p true -v" 
java $java_flags -cp "$classpath" megan.tools.RMA2Info $options > $GENE_PRED/id_gene2seed.$PRJ_NAME.tab 

cd -

#convert to csv all files
sed -i 's/;/\t/g' $GENE_PRED/id_gene2tax.$PRJ_NAME.tab
sed -i '/^#/ d' $GENE_PRED/id_gene2tax.$PRJ_NAME.tab

sed -i 's/;/\t/g' $GENE_PRED/id_gene2seed.$PRJ_NAME.tab
sed -i 's/\tSEED//g' $GENE_PRED/id_gene2seed.$PRJ_NAME.tab

# #convert to csv
# sed -i 's/ //g' $GENE_PRED/id_gene2tax_names.$PRJ_NAME.tab 
# sed -i 's/\]/;/g' $GENE_PRED/id_gene2tax_names.$PRJ_NAME.tab 
# sed -i 's/\[//g' $GENE_PRED/id_gene2tax_names.$PRJ_NAME.tab 
# sed -i 's/\tSK/;SK/' $GENE_PRED/id_gene2tax_names.$PRJ_NAME.tab 


echo -e "Finished extracting infos"

echo -e "Producing file with the complete SEED paths"
sort -k2 $GENE_PRED/id_gene2seed.$PRJ_NAME.tab > $GENE_PRED/id_gene2seed_sorted.$PRJ_NAME.tab 
time python seed_processing.py $GENE_PRED/id_gene2seed_sorted.$PRJ_NAME.tab $DB/subsys_space.txt $GENE_PRED/id_gene2seed_sorted_path.$PRJ_NAME.tab 

sed 's/\t/;/g' $GENE_PRED/id_gene2seed_sorted_path.TEST.tab

echo -e  "`date`: Diamond output parsing Ended \n "
echo "-------------------------------------------------------------------------"
