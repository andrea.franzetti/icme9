# ICME 9 - International Course of Microbial Ecology, 2-7 September 2018

This repository has been specifically developed for ICME9 - International Course of Microbial Ecology (2-7 September 2018 in Bruxelles, Belgium)
For further information about the corse, visit http://www.microbeco.org/

The repository contains all the third-part software and dependencies to run a complete bioinformatic analysis of a metagenomic dataset. The analyses are supposed to be ran on MARCONI cluster of CINECA (https://www.cineca.it/it/content/marconi).

The following programs are used for the analysis:

Quality Control: FastQC

Quality Filtering: Sickle

Assembly: IDBA-UD

Gene prediction: Prodigal

Annotation: DIAMOND

Alignment: Bowtie2

Alignment file manipulation: Samtools, Bedtools

Taxonomic assignemts and parsing: MEGAN, in-house scripts

Graphical output: Krona 



Documentation for installing and running the analyses is avilable at: http://icme9.readthedocs.io/en/latest/


